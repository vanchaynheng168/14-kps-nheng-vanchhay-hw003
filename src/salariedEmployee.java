public class salariedEmployee extends staffMember {
    private double salary=0;
    private double bonus=0;

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    public double getBonus() {
        return bonus;
    }

    public void setBonus(double bonus) {
        this.bonus = bonus;
    }
    public salariedEmployee(){

    }
    public salariedEmployee(int id, String name, String address, double salary, double bonus,double pay) {
        super(id, name, address);
        this.salary = salary;
        this.bonus = bonus;
        this.pay(salary,bonus);
    }
    public double pay(double salary, double bonus){
        double payment =0;
        salary=salary<=0?0:salary;
        bonus=bonus<=0?0:bonus;
        if(salary==0||bonus==0){
            payment=0;
        }else {
            payment=salary+bonus;
        }
        return payment;
    }
    @Override
    public String toString(){
        return  super.toString()+"Salary: "+ salary+"\n"+ "Bonus: "+bonus +"\n"+"Payment: "+pay(salary,bonus);
    }
}

public class hourlyEmployee extends staffMember{
    private double hourWork=0;
    private double rate =0;
    public double getHourWork() {
        return hourWork;
    }
    public void setHourWork(double hourWork) {
        this.hourWork = hourWork;
    }
    public double getRate() {
        return rate;
    }
    public void setRate(double rate) {
        this.rate = rate;
    }
    public hourlyEmployee()
    {

    }
    public hourlyEmployee(int id, String name, String address, double hourWork, double rate,double pay) {
        super(id, name, address);
        this.hourWork = hourWork;
        this.rate = rate;
        this.pay(hourWork,rate);
    }
    public double pay(double hourWork, double rate){
        double payment =0;
        hourWork=hourWork<=0?0:hourWork;
        rate=rate<=0?0:rate;
        if(hourWork==0||hourWork==0){
            payment=0;
        }else {
            payment=hourWork*rate;
        }
        return payment;
    }
    @Override
    public String toString(){
        return super.toString()+"Hour Worked: "+ hourWork+"\n"+"Rate: "+ rate+"\n"+ "Payment: "+pay(hourWork,rate);
    }


}

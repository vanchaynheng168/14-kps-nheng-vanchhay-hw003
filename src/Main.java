import javafx.beans.binding.ObjectExpression;

import java.lang.reflect.Array;
import java.util.*;

public class Main {

    static void showList(List<Object> lsEmp){
        //Print all Item
//        Collections.sort(lsEmp,StuNameComparator);
        lsEmp.sort(new ShortName());
        Iterator iterator = lsEmp.iterator();
        while (iterator.hasNext()){
            System.out.println(iterator.next());
            System.out.println("_______________________________________");
            System.out.println();
        }
    }
    static int validate(String input){

        int t=0;
        boolean correct_input;
        do {
            String digit = "\\d+";
            correct_input = input.matches(digit);
            if(correct_input){
                t=Integer.parseInt(input);
            }
            if (!(correct_input && t!=0)){
                System.out.println("Input only number !!");
                System.out.println();
            }
            return t;
        } while (!(correct_input && t!=0));
    }
    static double validate_double(String input){

        double t=0;
        boolean correct_input;
        do {
            String digit = "\\d+";
            correct_input = input.matches(digit);
            if(correct_input){
                t=Integer.parseInt(input);

            }
            if (!(correct_input && t!=0)){
                System.out.println("Input only number !!");
                System.out.println();
            }
            return t;
        } while (!(correct_input && t!=0));
    }

    public static void main(String[] args){
        Scanner input = new Scanner(System.in).useDelimiter("\n");
        int id = 0;
        String name = "";
        String address = "";
        double salary = 0;
        double bonus = 0;
        double hourWork = 0;
        double rate = 0;
        int opt=0;

        int idEmp=0;
        int idSalariedEmployee=0;
        int idVolunteer=0;
          List<Object> lsEmp = new ArrayList<Object>();

        lsEmp.add(new salariedEmployee(1,"Banchay","PP",-450,20,470));
        lsEmp.add(new hourlyEmployee(3,"Cros","PP",50,20,1000));
        lsEmp.add(new volunteer(2,"Aada","PP"));
        System.out.println();
        while (true){
            //Print all Item
            showList(lsEmp);

            System.out.println("1). Add Employee"+"\t"+"2). Edit"+"\t"+"3). Remove"+"\t"+"4). Exit");

                System.out.print("Choose Option(1-4): ");
                opt=validate(input.nextLine());

            if(opt==1){
                boolean b=true;
                while (b==true){
//                    System.out.println("_______________________________________");
                    System.out.println("1). Volunteer"+"\t"+"2). Hourly Emp"+"\t"+"3). Salaried Emp"+"\t"+"4). Back");
                    System.out.println("Choose Option: ");
                    int opt1 = validate(input.nextLine());
                    System.out.println("===================INSERT INFO===================");
                    if(opt1==1){
                        //Emp Volunteer
                        volunteer volunteers = new volunteer();
                        System.out.print("Enter Staff Member's ID: ");
                        id = input.nextInt();
                        volunteers.setId(id);
                        System.out.print("Enter Staff Member's Name: ");
                        name = input.next();
                        volunteers.setName(name.substring(0,1).toUpperCase()+ name.substring(1));
                        System.out.print("Enter Staff Member's Address: ");
                        address = input.next();
                        volunteers.setAddress(address);
                        lsEmp.add(volunteers);
                        System.out.println();
                    }
                    else if(opt1==2){
                        //emp Hour worked
                        hourlyEmployee hourlyEmployee = new hourlyEmployee();
                        System.out.print("Enter Staff Member's ID: ");
                        id = input.nextInt();
                        hourlyEmployee.setId(id);
                        System.out.print("Enter Staff Member's Name: ");
                        name = input.next();
                        hourlyEmployee.setName(name.substring(0,1).toUpperCase()+ name.substring(1));
                        System.out.print("Enter Staff Member's Address: ");
                        address = input.next();
                        hourlyEmployee.setAddress(address);
                        System.out.print("Enter Staff Member's Hour Worked: ");
                        hourWork = validate_double(input.nextLine());
                        hourlyEmployee.setHourWork(hourWork);
                        System.out.print("Enter Staff Member's Rate: ");
                        rate = validate_double(input.nextLine());
                        hourlyEmployee.setRate(rate);
                        lsEmp.add(hourlyEmployee);
                        System.out.println();
                    }
                    else if(opt1==3){
                        //Employee Salary
                        salariedEmployee employee = new salariedEmployee();
                        System.out.print("Enter Staff Member's ID: ");
                        id = input.nextInt();
                        employee.setId(id);
                        System.out.print("Enter Staff Member's Name: ");
                        name = input.next();
                        employee.setName(name.substring(0,1).toUpperCase()+ name.substring(1));
                        System.out.print("Enter Staff Member's Address: ");
                        address = input.next();
                        employee.setAddress(address);
                        System.out.print("Enter Staff Member's Salary: ");
                        salary = validate_double(input.next());
                        if(salary>0){
                            employee.setSalary(salary);
                        }
                        System.out.print("Enter Staff Member's Bonus: ");
                        bonus = validate_double(input.next());
                        employee.setBonus(bonus);
                        lsEmp.add(employee);
                        System.out.println();
                    }
                    else if(opt1==4){
                        b=false;
                    }
                    showList(lsEmp);
                }
            }
//            String name_new=null;
            else if (opt==2){
                System.out.println("===================INSERT INFO===================");
                System.out.println("Enter Employee ID to Update: ");
                int input_id = input.nextInt();

                for( Object item: lsEmp ) {
                    if( item instanceof hourlyEmployee) {
                        idEmp = ((hourlyEmployee) item).getId();
                            if (idEmp == input_id){
                            System.out.println(item);
                            System.out.println("============NEW INFORMATION OF STAFF MEMBER============");
                            System.out.print("Enter Staff Member's Name: ");
                            name=input.next();
                            System.out.print("Enter Staff Member's Hour Work: ");
                            hourWork=validate_double(input.next());
                            System.out.print("Enter Staff Member's Rate: ");
                            rate=validate_double(input.next());
                            ((hourlyEmployee) item).setName(name);
                            ((hourlyEmployee) item).setRate(rate);
                            ((hourlyEmployee) item).setHourWork(hourWork);

                            }

                    } else if( item instanceof salariedEmployee) {
                        idSalariedEmployee = ((salariedEmployee) item).getId();
                        if (idSalariedEmployee == input_id)
                        {
                            System.out.println(item);
                            System.out.println("============NEW INFORMATION OF STAFF MEMBER============");
                            System.out.print("Enter Staff Member's Name: ");
                            name=input.next();
                            System.out.print("Enter Staff Member's Salary: ");
                            salary=validate_double(input.next());
                            System.out.print("Enter Staff Member's Bonus: ");
                            bonus=validate_double(input.next());
                            ((salariedEmployee) item).setName(name);
                            ((salariedEmployee) item).setSalary(salary);
                            ((salariedEmployee) item).setBonus(bonus);

                        }
                    }
                    else if( item instanceof volunteer) {
                        idVolunteer = ((volunteer) item).getId();
                        if (idVolunteer == input_id){
                            System.out.println(item);
                            System.out.println("============NEW INFORMATION OF STAFF MEMBER============");
                            System.out.print("Enter Staff Member's Name: ");
                            name=input.next();
                            ((volunteer) item).setName(name);
                        }
                    }
                }
            }
            else if(opt==3){
                System.out.println("===================Remove INFO===================");
                System.out.println("Enter Employee ID to Remove: ");
                int input_id = input.nextInt();
                for( Object item: lsEmp ) {
                    if( item instanceof hourlyEmployee) {

                        for(int i=0;i<lsEmp.size();i++){
                            if (idEmp == input_id){
                                input_id = idEmp;
                                lsEmp.remove(i);
                                break;
                            }
                        }

                    } else if( item instanceof salariedEmployee) {
                        idSalariedEmployee = ((salariedEmployee) item).getId();
                        if (idSalariedEmployee == input_id){

                            for(int i=0;i<lsEmp.size();i++){
                                if (idSalariedEmployee == input_id){
                                    input_id = idSalariedEmployee;
                                    lsEmp.remove(i);
                                    break;
                                }
                            }
                        }
                    }
                    else if( item instanceof volunteer) {
                        idVolunteer = ((volunteer) item).getId();
                        if (idVolunteer == input_id){

                            for(int i=0;i<lsEmp.size();i++){
                                if (idVolunteer == input_id){
                                    input_id = idVolunteer;
                                    lsEmp.remove(i);
                                    break;
                                }
                            }
                        }
                    }
                }
            }
            else if(opt==4){
                System.out.println("Good Bye!!");
                System.out.close();
            }
        }
    }
}

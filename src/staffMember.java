import java.io.Serializable;
import java.util.Comparator;

public class staffMember implements Serializable {
    protected int id;
    protected String name=null;
    protected String address;

    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public String getAddress() {
        return address;

    }
    public void setAddress(String address) {
        this.address = address;
    }
    public staffMember(){
    }
    public staffMember(int id, String name, String address) {
        this.id = id;
        this.name = name;
        this.address = address;
    }
//    public static Comparator<staffMember> NameComparator = new Comparator<staffMember>() {
//
//        public int compare(staffMember s1, staffMember s2) {
//            String Name1 = s1.getName().toUpperCase();
//            String Name2 = s2.getName().toUpperCase();
//
//            //ascending order
//            return Name1.compareTo(Name2);
//
//            //descending order
//            //return StudentName2.compareTo(StudentName1);
//        }};

    @Override
    public String toString(){
        return "Id: "+id+"\n"+ "Name: "+name+"\n"+"Address: "+ address+"\n";
    }

}

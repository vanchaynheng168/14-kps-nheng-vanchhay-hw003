import java.util.Comparator;
public class ShortName implements Comparator<Object>{
    @Override
    public int compare(Object o1, Object o2) {
        return o2.getClass().getName().compareToIgnoreCase(o1.getClass().getName());
    }
}
